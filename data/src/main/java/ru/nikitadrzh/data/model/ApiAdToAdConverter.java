package ru.nikitadrzh.data.model;

import java.util.ArrayList;
import java.util.List;

import ru.nikitadrzh.data.model.ApiAd.Content;
import ru.nikitadrzh.domain.model.Ad;

public class ApiAdToAdConverter {

    public List<Ad> mapApiAdToAd(ApiAd apiAdsToMap) {

        List<Ad> ads = new ArrayList<>();
        for (Content content : apiAdsToMap.content) {
            ads.add(new Ad(content.info, content.photo_src, content.seller.name,
                    content.seller.photoMax, content.seller.city, content.seller.vkId,
                    content.vkGroup.name, content.url, content.category,
                    content.timestamp.toString(), content.id, false));
        }
        return ads;
    }
}
