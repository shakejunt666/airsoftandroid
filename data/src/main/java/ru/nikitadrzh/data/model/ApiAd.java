package ru.nikitadrzh.data.model;

import java.util.List;

/**
 * POJO ApiModel
 */

public class ApiAd {
    public List<Content> content;
    public Pageable pageable;
    public Boolean last;
    public Integer totalPages;
    public Integer totalElements;
    public Boolean first;
    public Sort_ sort;
    public Integer numberOfElements;
    public Integer size;
    public Integer number;

    public class Content {
        public Integer id;
        public Seller seller;
        public VkAlbum vkAlbum;
        public String category;
        public Integer photo_id;
        public String url;
        public String photo_src;
        public String info;
        public Integer timestamp;
        public VkGroup vkGroup;
        public List<Object> duplicates;
        public Boolean deleted;
    }

    public class VkAlbum {
        public Integer vkId;
        public String name;
        public String category;
        public String status;
        public Boolean deleted;
    }

    public class VkGroup {
        public Integer vkId;
        public String name;
        public String status;
        public List<Album> albums;
        public Boolean deleted;
    }

    public class Album {
        public Integer vkId;
        public String name;
        public String category;
        public String status;
        public Boolean deleted;
    }

    public class Seller {
        public Integer vkId;
        public String name;
        public String city;
        public String photoMax;
    }

    public class Pageable {
        public Sort sort;
        public Integer pageSize;
        public Integer pageNumber;
        public Integer offset;
        public Boolean paged;
        public Boolean unpaged;
    }

    public class Sort {
        public Boolean sorted;
        public Boolean unsorted;
    }

    public class Sort_ {
        public Boolean sorted;
        public Boolean unsorted;
    }
}

