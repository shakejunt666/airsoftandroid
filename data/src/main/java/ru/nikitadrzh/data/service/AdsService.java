package ru.nikitadrzh.data.service;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.nikitadrzh.data.model.ApiAd;

public interface AdsService {

    @GET("items")
    Single<ApiAd> getAds(@Query("page") int page);
}
