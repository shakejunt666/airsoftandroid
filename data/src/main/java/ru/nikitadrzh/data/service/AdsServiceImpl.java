package ru.nikitadrzh.data.service;

import io.reactivex.Single;
import ru.nikitadrzh.data.model.ApiAd;

public class AdsServiceImpl implements AdsService {

    private int currentPageNumber;

    private AdsService adsService = RetrofitInitializer.getInstance().getAdsService();

    @Override
    public Single<ApiAd> getAds(int page) {
        if (currentPageNumber != 0) {
            Single<ApiAd> apiAd = adsService.getAds(currentPageNumber);
            currentPageNumber++;
            return apiAd;
        }
        currentPageNumber++;
        return adsService.getAds(page);
    }
}
