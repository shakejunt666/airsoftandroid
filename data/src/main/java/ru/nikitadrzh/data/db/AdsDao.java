package ru.nikitadrzh.data.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.nikitadrzh.data.db.model.DbAd;

public interface AdsDao {
    Completable saveAd(DbAd dbAd);

    Single<List<DbAd>> getAds();

    Completable deleteAd(DbAd dbAd);
}
