package ru.nikitadrzh.data.db.model;

import java.util.ArrayList;
import java.util.List;

import ru.nikitadrzh.domain.model.Ad;

public class DbAdsToAdsConverter {

    public List<Ad> mapDbAdsToAds(List<DbAd> dbAdsToMap) {
        List<Ad> adList = new ArrayList<>();
        for (DbAd dbAdToMap : dbAdsToMap) {
            adList.add(new Ad(dbAdToMap.getDescription(), dbAdToMap.getPictureUrl(), dbAdToMap.getSellerName(),
                    dbAdToMap.getSellerUrl(), dbAdToMap.getSellerCity(), dbAdToMap.getSellerVkId(),
                    dbAdToMap.getVkGroupName(), dbAdToMap.getVkGroupUrl(), dbAdToMap.getCategory(),
                    dbAdToMap.getAddTime(), dbAdToMap.getId(), true));
        }
        return adList;
    }
}
