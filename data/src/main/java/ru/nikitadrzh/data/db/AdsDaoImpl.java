package ru.nikitadrzh.data.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.realm.Realm;
import ru.nikitadrzh.data.db.model.DbAd;

public class AdsDaoImpl implements AdsDao {

    @Override
    public Completable saveAd(DbAd dbAd) {
        return Completable.fromAction(() -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(dbAd));
            }
        });
    }

    @Override
    public Single<List<DbAd>> getAds() {
        return Single.create(emitter -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                realm.refresh();
                List<DbAd> dbAdList = realm.where(DbAd.class).findAll();
                emitter.onSuccess(realm.copyFromRealm(dbAdList));
            }
        });
    }

    @Override
    public Completable deleteAd(DbAd dbAd) {
        return Completable.fromAction(() -> {
            try (Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransaction(realm1 -> realm1
                        .where(DbAd.class)
                        .equalTo("id", dbAd.getId())
                        .findAll()
                        .deleteAllFromRealm());
            }
        });
    }
}
