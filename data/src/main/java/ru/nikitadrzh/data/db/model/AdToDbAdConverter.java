package ru.nikitadrzh.data.db.model;

import ru.nikitadrzh.domain.model.Ad;

public class AdToDbAdConverter {
    public DbAd mapAdToDbAd(Ad adToMap) {
        return new DbAd(adToMap.getId(), adToMap.getDescription(), adToMap.getPictureUrl(), adToMap.getSellerName(),
                adToMap.getSellerUrl(), adToMap.getSellerCity(), adToMap.getSellerVkId(),
                adToMap.getVkGroupName(), adToMap.getVkGroupUrl(), adToMap.getCategory(),
                adToMap.getAddTime());
    }
}
