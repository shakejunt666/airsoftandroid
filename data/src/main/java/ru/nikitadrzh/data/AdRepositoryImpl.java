package ru.nikitadrzh.data;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.nikitadrzh.data.db.AdsDao;
import ru.nikitadrzh.data.db.model.AdToDbAdConverter;
import ru.nikitadrzh.data.db.model.DbAdsToAdsConverter;
import ru.nikitadrzh.data.model.ApiAdToAdConverter;
import ru.nikitadrzh.data.service.AdsService;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

public class AdRepositoryImpl implements AdRepository {

    private AdsService adsService;
    private ApiAdToAdConverter apiAdToAdConverter;

    private AdsDao adsDao;
    private DbAdsToAdsConverter dbAdsToAdsConverter;
    private AdToDbAdConverter adToDbAdConverter;

    public AdRepositoryImpl(AdsService adsService, ApiAdToAdConverter apiAdToAdConverter,
                            AdsDao adsDao, DbAdsToAdsConverter dbAdsToAdsConverter,
                            AdToDbAdConverter adToDbAdConverter) {
        this.adsService = adsService;
        this.apiAdToAdConverter = apiAdToAdConverter;
        this.adsDao = adsDao;
        this.dbAdsToAdsConverter = dbAdsToAdsConverter;
        this.adToDbAdConverter = adToDbAdConverter;
    }

    /**
     * С сервера получаем ApiAd, конвертируем в Ad и пускаем дальше
     */
    @Override
    public Single<List<Ad>> getAds() {
        return adsService.getAds(0)
                .subscribeOn(Schedulers.io())
                .map(apiAdToAdConverter::mapApiAdToAd)
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Из бд получаем ApiAd, конвертируем в Ad и пускаем дальше
     */
    @Override
    public Single<List<Ad>> getFavouriteAds() {
        return adsDao.getAds()
                .map(dbAdsToAdsConverter::mapDbAdsToAds);
    }

    /**
     * Сохраняем в БД
     */
    @Override
    public Completable saveAd(Ad ad) {
        return adsDao.saveAd(adToDbAdConverter.mapAdToDbAd(ad));
    }

    /**
     * Удаляем из БД
     */
    @Override
    public Completable deleteAdFromFavourites(Ad ad) {
        return adsDao.deleteAd(adToDbAdConverter.mapAdToDbAd(ad));
    }
}
