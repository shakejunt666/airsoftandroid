package ru.nikitadrzh.data.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import ru.nikitadrzh.data.model.ApiAd;

public class AdsServiceImplTest {

    private AdsService adsService;
    private TestObserver<ApiAd> testObserver;

    @Before
    public void setUp() {
        adsService = Mockito.mock(AdsService.class);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldFetchAds() {
        ApiAd apiAd = new ApiAd();
        Mockito.when(adsService.getAds(1)).thenReturn(Single.just(apiAd));
        adsService.getAds(1).subscribe(testObserver);
        testObserver.assertSubscribed();
    }
}