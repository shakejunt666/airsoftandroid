package ru.nikitadrzh.airsoftsecondtry.ui.ads.list;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdViewModelMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.domain.interactor.ShowAdsUseCase;
import ru.nikitadrzh.domain.model.Ad;

public class AdsPresenterTest {
    @Mock
    ShowAdsUseCase showAdsUseCase;
    @Mock
    AdViewModelMapper adViewModelMapper;
    @InjectMocks
    AdsPresenter adsPresenter;

    private AdsContract.View view;

    @Before
    public void setUp() {

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(h -> Schedulers.trampoline());
        RxJavaPlugins.setIoSchedulerHandler(h -> Schedulers.trampoline());

        view = Mockito.mock(AdsContract.View.class);
        adsPresenter = new AdsPresenter(view);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFetchAdsAndPassThemToView() {

        List<Ad> ads = new ArrayList<>();
        Ad ad = new Ad("описание", "пикчЮрл", "СеллерНэйм",
                "СеллерЮрл", "ГруппаВВк", "ЮрлГрупп",
                "Категория", "ЭддТайм");
        ads.add(ad);

        List<AdViewModel> adViewModels = new ArrayList<>();
        AdViewModel adViewModel = new AdViewModel("описание", "пикчЮрл",
                "", "", "", "",
                "Категория", "ЭддТайм");
        adViewModels.add(adViewModel);

        Mockito.when(showAdsUseCase.execute()).thenReturn(Single.just(ads));
        Mockito.when(adViewModelMapper.mapAdToViewModel(ads)).thenReturn(adViewModels);

        adsPresenter.findAds();

        Mockito.verify(showAdsUseCase, Mockito.times(1)).execute();
        Mockito.verify(view, Mockito.times(1)).showAds(adViewModels);
    }
}
