package ru.nikitadrzh.airsoftsecondtry.ui.router;

import android.content.Intent;
import android.net.Uri;

public class IntentManagerImpl implements IntentManager {
    @Override
    public Intent getOpenVkIntent(Integer VkUrl) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/im?sel=" + VkUrl));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}
