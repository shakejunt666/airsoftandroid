package ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites;

import ru.nikitadrzh.airsoftsecondtry.ui.ads.BaseAdsContract;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsContract;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

public interface FavouriteAdsContract extends AdsContract {

    interface Presenter extends BaseAdsContract.Presenter {
        void deleteAdFromFavourites(AdViewModel adViewModel);
    }

    interface View extends BaseAdsContract.View {
        void showAdDeleted();
    }
}
