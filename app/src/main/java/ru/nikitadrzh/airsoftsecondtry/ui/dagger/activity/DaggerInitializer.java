package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity;

import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouritesFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.AdsPresenterModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.FavouriteAdsPresenterModule;

/**
 * Класс предоставляет компонент, чтоб вызывать inject
 */
public class DaggerInitializer {

    private static ActivityComponent component;

    public static void init(AdsFragment adsFragment, FavouritesFragment favouritesFragment) {
        component = DaggerActivityComponent.builder()
                .adsPresenterModule(new AdsPresenterModule(adsFragment))
                .favouriteAdsPresenterModule(new FavouriteAdsPresenterModule(favouritesFragment))
                .build();
    }

    public static ActivityComponent getActivityComponent() {
        return component;
    }
}
