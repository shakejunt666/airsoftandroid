package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouriteAdsContract;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouriteAdsPresenter;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.DaggerInitializer;

@Module
public class FavouriteAdsPresenterModule {

    public FavouriteAdsContract.View view;

    public FavouriteAdsPresenterModule(FavouriteAdsContract.View view) {
        this.view = view;
    }

    @Provides
    public FavouriteAdsContract.Presenter provideFavouriteAdsPresenter() {
        FavouriteAdsPresenter presenter = new FavouriteAdsPresenter(view);
        DaggerInitializer.getActivityComponent().inject(presenter);
        return presenter;
    }
}
