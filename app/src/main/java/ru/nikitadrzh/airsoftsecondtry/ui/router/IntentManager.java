package ru.nikitadrzh.airsoftsecondtry.ui.router;

import android.content.Intent;

public interface IntentManager {
    Intent getOpenVkIntent(Integer VkUrl);
}
