package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.model.ApiAdToAdConverter;
import ru.nikitadrzh.data.service.AdsService;
import ru.nikitadrzh.data.service.AdsServiceImpl;

@Module
public class ServiceModule {

    @Provides
    public AdsService providesAdsService() {
        //тут синглтон внутри могут быть проблемы
        return new AdsServiceImpl();
    }

    @Provides
    public ApiAdToAdConverter providesApiAdToAdConverter() {
        return new ApiAdToAdConverter();
    }
}
