package ru.nikitadrzh.airsoftsecondtry.ui.router;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.util.Objects;

import ru.nikitadrzh.airsoftsecondtry.R;
import ru.nikitadrzh.airsoftsecondtry.ui.MainActivity;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.full_photo_details.AdFullPhotoFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouritesFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;

/**
 * Класс, вызываемый активити для навигации между фрагментами
 */
public class FragmentNavigator {

    private FragmentManager fragmentManager;

    private IntentManager intentManager;

    private Context appContext;

    public FragmentNavigator(FragmentManager fragmentManager, Context appContext, IntentManager intentManager) {
        this.fragmentManager = fragmentManager;
        this.appContext = appContext;
        this.intentManager = intentManager;
    }

    public void showAdsFragment(AdsFragment adsFragment, FavouritesFragment favouritesFragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!adsFragment.isAdded()) {
            fragmentTransaction.hide(favouritesFragment);
            fragmentTransaction.add(R.id.adsfragment_container, adsFragment, "ADS");
            fragmentTransaction.show(adsFragment);
        } else {
            fragmentTransaction.hide(favouritesFragment);
            fragmentTransaction.replace(R.id.adsfragment_container, adsFragment).show(adsFragment);
        }
        fragmentTransaction.commit();
    }

    public void showAdsFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ((MainActivity) Objects.requireNonNull(fragment.getActivity())).showNavigationAndToolbar();
        fragmentTransaction.remove(fragment);
        fragmentManager.popBackStackImmediate();
        fragmentTransaction.commit();
    }

    public void showFavouriteAdsFragment(FavouritesFragment favouritesFragment,
                                         AdsFragment adsFragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!favouritesFragment.isAdded()) {
            fragmentTransaction.hide(adsFragment);
            fragmentTransaction.add(R.id.adsfragment_container, favouritesFragment, "FAV")
                    .show(favouritesFragment);
        } else {
            fragmentTransaction.hide(adsFragment);
            fragmentTransaction.show(favouritesFragment);
        }
        fragmentTransaction.commit();
    }

    public void showAdFullPhotoFragment(Fragment fragment) {
        AdFullPhotoFragment fullPhotoFragment = new AdFullPhotoFragment();

        Bundle photoBundle = new Bundle();
        photoBundle.putString("picture", fragment.getArguments().getString("picture"));
        fullPhotoFragment.setArguments(photoBundle);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(fragment);
        fragmentTransaction.addToBackStack("details");
        fragmentTransaction.add(R.id.adsfragment_container, fullPhotoFragment, "fullPhotoFragment")
                .show(fullPhotoFragment);
        fragmentTransaction.commit();
    }

    public void showDialogWithSeller(int id) {
        appContext.startActivity(intentManager.getOpenVkIntent(id));
    }
}
