package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.db.AdsDao;
import ru.nikitadrzh.data.db.AdsDaoImpl;
import ru.nikitadrzh.data.db.model.AdToDbAdConverter;
import ru.nikitadrzh.data.db.model.DbAdsToAdsConverter;

@Module
public class DbModule {

    @Provides
    public AdsDao providesAdsDao() {
        return new AdsDaoImpl();
    }

    @Provides
    public DbAdsToAdsConverter providesDbAdToAdConverter() {
        return new DbAdsToAdsConverter();
    }

    @Provides
    public AdToDbAdConverter providesAdToDbAdConverter() {
        return new AdToDbAdConverter();
    }
}
