package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.data.AdRepositoryImpl;
import ru.nikitadrzh.data.db.AdsDao;
import ru.nikitadrzh.data.db.model.AdToDbAdConverter;
import ru.nikitadrzh.data.db.model.DbAdsToAdsConverter;
import ru.nikitadrzh.data.model.ApiAdToAdConverter;
import ru.nikitadrzh.data.service.AdsService;
import ru.nikitadrzh.domain.interactor.DeleteAdFromFavouritesUseCase;
import ru.nikitadrzh.domain.interactor.SaveAdUseCase;
import ru.nikitadrzh.domain.interactor.ShowAdsUseCase;
import ru.nikitadrzh.domain.interactor.ShowFavouriteAdsUseCase;
import ru.nikitadrzh.domain.repository.AdRepository;

@Module
public class UseCaseModule {

    @Provides
    public AdRepository providesAdRepository(AdsService service, ApiAdToAdConverter converter,
                                             AdsDao adsDao, DbAdsToAdsConverter dbAdsToAdsConverter,
                                             AdToDbAdConverter adToDbAdConverter) {
        return new AdRepositoryImpl(service, converter, adsDao, dbAdsToAdsConverter, adToDbAdConverter);
    }

    @Provides
    public ShowAdsUseCase providesShowAdsUseCase(AdRepository adRepository,
                                                 ShowFavouriteAdsUseCase showFavouriteAdsUseCase) {
        return new ShowAdsUseCase(adRepository, showFavouriteAdsUseCase);
    }

    @Provides
    public SaveAdUseCase provideSaveAdUseCase(AdRepository adRepository) {
        return new SaveAdUseCase(adRepository);
    }

    @Provides
    public ShowFavouriteAdsUseCase provideShowFavouriteAdsUseCase(AdRepository adRepository) {
        return new ShowFavouriteAdsUseCase(adRepository);
    }

    @Provides
    public DeleteAdFromFavouritesUseCase provideDeleteAdFromFavouritesUseCase(AdRepository adRepository) {
        return new DeleteAdFromFavouritesUseCase(adRepository);
    }
}
