package ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdViewModelMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManager;
import ru.nikitadrzh.domain.interactor.DeleteAdFromFavouritesUseCase;
import ru.nikitadrzh.domain.interactor.ShowFavouriteAdsUseCase;

public class FavouriteAdsPresenter implements FavouriteAdsContract.Presenter {
    private FavouriteAdsContract.View view;

    @Inject
    ShowFavouriteAdsUseCase showFavouriteAdsUseCase;

    @Inject
    DeleteAdFromFavouritesUseCase deleteAdFromFavouritesUseCase;

    @Inject
    AdViewModelMapper adViewModelMapper;

    @Inject
    AdMapper adMapper;

    @Inject
    IntentManager intentManager;

    @Inject
    PublishSubject<AdViewModel> subject;

    public FavouriteAdsPresenter(FavouriteAdsContract.View view) {
        this.view = view;
    }

    @Override
    public void deleteAdFromFavourites(AdViewModel adViewModel) {
        Disposable disposable = deleteAdFromFavouritesUseCase.execute(adMapper.mapAdViewModelToAd(adViewModel))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> notifyAdDeleteCompleted(adViewModel), this::notifyAdDeleteError);
    }

    @Override
    public void findAds() {
        Disposable disposable = showFavouriteAdsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .map(ads -> adViewModelMapper.mapAdToViewModel(ads))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showAds, this::showError);
    }

    @Override
    public void connectWithSeller(AdViewModel adViewModel) {
        view.showDialogWithSeller(intentManager.getOpenVkIntent(adViewModel.getSellerVkId()));
    }

    private void showAds(List<AdViewModel> adViewModels) {
        view.showAds(adViewModels);
    }

    private void showError(Throwable throwable) {
        view.showError(throwable);
    }

    private void notifyAdDeleteCompleted(AdViewModel adViewModel) {
        subject.onNext(adViewModel);
        view.showAdDeleted();
    }

    private void notifyAdDeleteError(Throwable throwable) {
        subject.onError(throwable);
        view.showError(throwable);
    }
}
