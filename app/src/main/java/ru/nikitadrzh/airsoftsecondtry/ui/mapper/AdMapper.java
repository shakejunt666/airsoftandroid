package ru.nikitadrzh.airsoftsecondtry.ui.mapper;

import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.domain.model.Ad;

public interface AdMapper {
    Ad mapAdViewModelToAd(AdViewModel adViewModelToAd);

}
