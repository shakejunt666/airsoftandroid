package ru.nikitadrzh.airsoftsecondtry.ui.ads.list;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import ru.nikitadrzh.airsoftsecondtry.R;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.details.AdDetailsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.DaggerInitializer;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

public class AdsFragment extends Fragment implements AdsContract.View {

    @Inject
    public AdsContract.Presenter presenter;

    @BindView(R.id.ads_recycler)
    RecyclerView adsRecyclerView;

    @BindView(R.id.progress_bar_ads)
    ProgressBar progressBar;

    private AdsAdapter adsAdapter;

    private RecyclerView.LayoutManager adsLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInitializer.getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.ads_fragment, container, false);
        ButterKnife.bind(this, fragmentView);
        progressBar.setVisibility(View.VISIBLE);
        initRecyclerView();
        presenter.findAds();
        return fragmentView;
    }

    @Override
    public void onPause() {
        super.onPause();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void showAds(List<AdViewModel> adViewModels) {
        adsAdapter.onAdsUpdate(adViewModels);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(Throwable error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialogWithSeller(Intent intent) {
        Objects.requireNonNull(getContext()).startActivity(intent);
    }

    @Override
    public void showAdSaved(AdViewModel adViewModel) {
        Toast.makeText(getContext(), "Ad was added to favourites", Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateAdsList(AdViewModel adViewModel) {
        adsAdapter.updateList(adViewModel);
    }

    private void initRecyclerView() {
        if (adsRecyclerView.getAdapter() == null) {
            adsAdapter = new AdsAdapter();
            adsRecyclerView.setAdapter(adsAdapter);
        } else {
            adsAdapter = (AdsAdapter) adsRecyclerView.getAdapter();
        }

        Disposable disposable = adsAdapter.onSellerImageClick()
                .subscribe(this::onSellerImageClicked);

        Disposable disposable2 = adsAdapter.onFavouriteImageClick()
                .subscribe(this::onFavouriteImageClicked);

        Disposable disposable3 = adsAdapter.getOnScrolledToUpdateListSubject()
                .subscribe(this::showMoreAds);

        Disposable disposable4 = adsAdapter.onCardClick()
                .subscribe(this::showDetailsView);

        adsLayoutManager = new LinearLayoutManager(null);
        adsRecyclerView.setLayoutManager(adsLayoutManager);
    }

    private void showMoreAds(String msg) {
        progressBar.setVisibility(View.VISIBLE);
        presenter.findAds();
    }

    private void onSellerImageClicked(AdViewModel adViewModel) {
        presenter.connectWithSeller(adViewModel);
    }

    private void onFavouriteImageClicked(AdViewModel adViewModel) {
        if (!adViewModel.isFavourite()) {
            presenter.saveAdToFavourites(adViewModel);
        } else {
            presenter.deleteAdFromFavourites(adViewModel);
        }
    }

    private void showDetailsView(AdViewModel adViewModel) {
        FragmentTransaction fragmentTransaction = null;
        AdDetailsFragment fragment = new AdDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("picture", adViewModel.getPictureUrl());
        bundle.putString("name", adViewModel.getSellerName());
        bundle.putString("city", adViewModel.getSellerCity());
        bundle.putString("avatar", adViewModel.getSellerUrl());
        bundle.putString("description", adViewModel.getDescription());
        bundle.putString("group", adViewModel.getVkGroupName());
        bundle.putString("group_url", adViewModel.getVkGroupUrl());
        bundle.putString("time", adViewModel.getAddTime());
        bundle.putInt("sellerVkId", adViewModel.getSellerVkId());
        fragment.setArguments(bundle);

        if (getFragmentManager() != null) {
            fragmentTransaction = getFragmentManager().beginTransaction();
        }
        fragmentTransaction.hide(this);
        fragmentTransaction.add(R.id.adsfragment_container, fragment, "details");
        fragmentTransaction.show(fragment);
        fragmentTransaction.addToBackStack("ADS");

        fragmentTransaction.commit();
    }
}
