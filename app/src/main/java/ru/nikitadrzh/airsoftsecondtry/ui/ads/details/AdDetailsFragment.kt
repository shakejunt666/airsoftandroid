package ru.nikitadrzh.airsoftsecondtry.ui.ads.details

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.ads_details_fragment.*
import kotlinx.android.synthetic.main.ads_details_fragment_bottomsheet.*
import ru.nikitadrzh.airsoftsecondtry.R
import ru.nikitadrzh.airsoftsecondtry.ui.MainActivity
import ru.nikitadrzh.airsoftsecondtry.ui.router.FragmentNavigator
import java.sql.Timestamp
import javax.inject.Inject

class AdDetailsFragment : Fragment() {

    @Inject
    lateinit var presenter: AdDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity?.window?.setFlags(
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as MainActivity).hideNavigationAndToolbar()
        return inflater.inflate(R.layout.ads_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val behavior = BottomSheetBehavior.from(bottom_sheet)
        behavior.isHideable = false

        val bundle = this.arguments
        Glide.with(this)
                .load(bundle?.getString("picture"))
                .placeholder(R.drawable.camo3)
                .into(merch_picture)

        Glide.with(this)
                .load(bundle?.getString("avatar"))
                .into(seller_image)

        seller_name.text = bundle?.getString("name") ?: ""
        seller_city.text = bundle?.getString("city") ?: ""
        description_info.text = bundle?.getString("description") ?: ""
        shop_info.text = bundle?.getString("group") ?: ""

        date_info.text = bundle?.getString("time") ?: ""

        val secondsInDay = 86400
        val dateAdd = bundle?.getString("time")
        val dateDays = Integer.parseInt(dateAdd!!) / secondsInDay

        val dateCurrent = (Timestamp(System.currentTimeMillis()).time / 1000).toString()
        val currentDays = Integer.parseInt(dateCurrent) / secondsInDay

        val result = when (currentDays - dateDays) {
            0 -> "Добавлено сегодня"
            1 -> "Добавлено вчера"
            2 -> "Добавлено позавчера"
            else -> "Добавлено давным-давно"
        }
        date_info.text = result

        back_button.setOnClickListener {
            activity?.onBackPressed()
        }

        merch_picture.setOnClickListener {
            //todo переходить по ссылочку интентом в вк по объяве
            val VkUrl = bundle.getString("group_url")
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/$VkUrl"))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.startActivity(intent)
        }

        messageBtn.setOnClickListener {
            //вызов презентора и метода "showDialog", передает ему id юзера
            if (bundle != null) {
                sendVkIntent(bundle)
//                presenter.showSellerDialog(bundle.getInt("sellerVkId"))
            }
        }

        seller_image.setOnClickListener {
            //вызов презентора и метода "showDialog", передает ему id юзера
            if (bundle != null) {
                sendVkIntent(bundle)
//                presenter.showSellerDialog(bundle.getInt("sellerVkId"))
            }
        }
    }

    private fun sendVkIntent(bundle: Bundle) {
        val VkUrl = bundle.getInt("sellerVkId")
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/im?sel=$VkUrl"))
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.startActivity(intent)
    }
}