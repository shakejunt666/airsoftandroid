//package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;
//
//import android.content.Context;
//
//import dagger.Module;
//import dagger.Provides;
//import ru.nikitadrzh.airsoftsecondtry.ui.router.FragmentNavigator;
//import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManager;
//
//@Module
//public class NavigationModule {
//
//    @Provides
//    FragmentNavigator provideFragmentNavigator(Context appContext, IntentManager intentManager) {
//        return new FragmentNavigator();
//    }
//}
