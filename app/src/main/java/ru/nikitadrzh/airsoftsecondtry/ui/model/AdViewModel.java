package ru.nikitadrzh.airsoftsecondtry.ui.model;

public class AdViewModel {
    private String description;
    private String pictureUrl;
    private String sellerName;
    private String sellerUrl;
    private String sellerCity;
    private Integer sellerVkId;
    private String vkGroupName;
    private String vkGroupUrl;
    private String category;
    private String addTime;
    private int id;
    private boolean isFavourite;

    public AdViewModel(String description,
                       String pictureUrl,
                       String sellerName,
                       String sellerUrl,
                       String sellerCity,
                       Integer sellerVkId,
                       String vkGroupName,
                       String vkGroupUrl,
                       String category,
                       String addTime,
                       int id,
                       boolean isFavourite) {
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.sellerName = sellerName;
        this.sellerUrl = sellerUrl;
        this.sellerCity = sellerCity;
        this.sellerVkId = sellerVkId;
        this.vkGroupName = vkGroupName;
        this.vkGroupUrl = vkGroupUrl;
        this.category = category;
        this.addTime = addTime;
        this.id = id;
        this.isFavourite = isFavourite;
    }

    public String getDescription() {
        return description;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Integer getSellerVkId() {
        return sellerVkId;
    }

    public String getSellerUrl() {
        return sellerUrl;
    }

    public String getVkGroupName() {
        return vkGroupName;
    }

    public String getVkGroupUrl() {
        return vkGroupUrl;
    }

    public String getCategory() {
        return category;
    }

    public String getAddTime() {
        return addTime;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AdViewModel{" +
                "description='" + description + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", sellerName='" + sellerName + '\'' +
                ", sellerUrl='" + sellerUrl + '\'' +
                ", vkGroupName='" + vkGroupName + '\'' +
                ", vkGroupUrl='" + vkGroupUrl + '\'' +
                ", category='" + category + '\'' +
                ", addTime='" + addTime + '\'' +
                '}';
    }
}
