package ru.nikitadrzh.airsoftsecondtry.ui.ads.details

import ru.nikitadrzh.airsoftsecondtry.ui.router.FragmentNavigator

class AdDetailsPresenter(private val navigator: FragmentNavigator) {

    fun showSellerDialog(sellerVkId: Int) {
        //презентер вызывает навигатор и метод "showDialogWithSeller"
        navigator.showDialogWithSeller(sellerVkId)
    }
}