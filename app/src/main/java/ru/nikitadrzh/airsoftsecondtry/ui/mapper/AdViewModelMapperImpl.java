package ru.nikitadrzh.airsoftsecondtry.ui.mapper;

import java.util.ArrayList;
import java.util.List;

import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.domain.model.Ad;

/**
 * Здесь прописывается логика, как Модель конвертируется во Вью модель
 */
public class AdViewModelMapperImpl implements AdViewModelMapper {
    @Override
    public List<AdViewModel> mapAdToViewModel(List<Ad> adsToMap) {
        List<AdViewModel> adViewModels = new ArrayList<>();
        for (Ad ad : adsToMap) {
            adViewModels.add(new AdViewModel(ad.getDescription(), ad.getPictureUrl(),
                    ad.getSellerName(), ad.getSellerUrl(), ad.getSellerCity(), ad.getSellerVkId(),
                    ad.getVkGroupName(), ad.getVkGroupUrl(), ad.getCategory(), ad.getAddTime(), ad.getId(),
                    ad.isFavourite()));
        }
        return adViewModels;
    }
}
