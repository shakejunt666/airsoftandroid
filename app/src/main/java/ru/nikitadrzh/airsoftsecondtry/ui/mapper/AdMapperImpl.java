package ru.nikitadrzh.airsoftsecondtry.ui.mapper;

import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.domain.model.Ad;

public class AdMapperImpl implements AdMapper {

    @Override
    public Ad mapAdViewModelToAd(AdViewModel adViewModelToAd) {
        return new Ad(adViewModelToAd.getDescription(), adViewModelToAd.getPictureUrl(),
                adViewModelToAd.getSellerName(), adViewModelToAd.getSellerUrl(),
                adViewModelToAd.getSellerCity(), adViewModelToAd.getSellerVkId(),
                adViewModelToAd.getVkGroupName(), adViewModelToAd.getVkGroupUrl(),
                adViewModelToAd.getCategory(), adViewModelToAd.getAddTime(), adViewModelToAd.getId(),
                adViewModelToAd.isFavourite());
    }
}
