package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.subjects.PublishSubject;
import ru.nikitadrzh.airsoftsecondtry.ui.MainActivity;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsContract;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsPresenter;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.DaggerInitializer;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdMapperImpl;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdViewModelMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdViewModelMapperImpl;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

@Module
public class AdsPresenterModule {

    public AdsContract.View view;

    public AdsPresenterModule(AdsContract.View view) {
        this.view = view;
    }

    @Provides
    public AdsContract.Presenter provideAdsPresenter() {
        AdsPresenter presenter = new AdsPresenter(view);
        DaggerInitializer.getActivityComponent().inject(presenter);
        return presenter;
    }

    @Provides
    public AdViewModelMapper providesAdViewModelMapper() {
        return new AdViewModelMapperImpl();
    }

    @Provides
    public AdMapper providesAdMapper() {
        return new AdMapperImpl();
    }

    @Provides
    @Singleton
    public PublishSubject<AdViewModel> providesPublishSubject() {
        return PublishSubject.create();
    }
}
