package ru.nikitadrzh.airsoftsecondtry.ui.ads.list;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.CompletableSource;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.CompletableSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import ru.nikitadrzh.airsoftsecondtry.R;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

/**
 * Адаптер для RecyclerView
 */
public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.AdsViewHolder> {

    private static final long CLICK_THROTTLE_WINDOW_MILLIS = 300L;

    private List<AdViewModel> adViewModels = new ArrayList<>();
    private Subject<AdViewModel> onSellerImageClickSubject = BehaviorSubject.create();
    private Subject<AdViewModel> onFavouriteImageClickSubject = BehaviorSubject.create();
    private Subject<AdViewModel> onCardClickSubject = BehaviorSubject.create();
    private PublishSubject<String> onScrolledToUpdateListSubject = PublishSubject.create();
    private boolean updatedNow = false;

    @NonNull
    @Override
    public AdsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.ad_cardview, viewGroup, false);
        return new AdsViewHolder(itemView, onSellerImageClickSubject, onFavouriteImageClickSubject,
                onCardClickSubject);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsViewHolder adsViewHolder, int i) {
        adsViewHolder.setItem(adViewModels.get(i));
        if (i == getItemCount() - 5 && !updatedNow) {
            onScrolledToUpdateListSubject.onNext("has scrolled to update");
            updatedNow = true;
        }
    }

    @Override
    public int getItemCount() {
        return adViewModels.size();
    }

    void onAdsUpdate(List<AdViewModel> adViewModels) {
        this.adViewModels.addAll(adViewModels);
        notifyDataSetChanged();
        updatedNow = false;
    }

    public void onSavedAdsUpdate(List<AdViewModel> adViewModels) {
        this.adViewModels = adViewModels;
        notifyDataSetChanged();
    }

    void updateList(AdViewModel adViewModel) {
        AdViewModel foundedAdInList = checkAdInList(adViewModel);

        if (foundedAdInList != null) {
            int index = adViewModels.indexOf(foundedAdInList);
            if (adViewModel.isFavourite()) {
                adViewModels.get(index).setFavourite(false);
            } else {
                adViewModels.get(index).setFavourite(true);
            }
            notifyDataSetChanged();
        }
    }

    private AdViewModel checkAdInList(AdViewModel adViewModel) {
        AdViewModel foundedAdInList = null;
        for (AdViewModel ad : adViewModels) {
            if (ad.getId() == adViewModel.getId()) {
                foundedAdInList = ad;
                break;
            }
        }
        return foundedAdInList;
    }

    Observable<String> getOnScrolledToUpdateListSubject() {
        return onScrolledToUpdateListSubject;
    }

    public Observable<AdViewModel> onCardClick() {
        return onCardClickSubject;
    }

    public Observable<AdViewModel> onSellerImageClick() {
        return onSellerImageClickSubject
                .throttleFirst(CLICK_THROTTLE_WINDOW_MILLIS, TimeUnit.MILLISECONDS);
    }

    public Observable<AdViewModel> onFavouriteImageClick() {
        return onFavouriteImageClickSubject
                .throttleFirst(CLICK_THROTTLE_WINDOW_MILLIS, TimeUnit.MILLISECONDS);
    }

    static final class AdsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.seller_name_text_view)
        TextView sellerName;

        @BindView(R.id.seller_city_text_view)
        TextView sellerCity;

        @BindView(R.id.date_view)
        TextView dateView;

        @BindView(R.id.ad_imageview)
        ImageView adImage;

        @BindView(R.id.favourite_button)
        ImageView favButton;

        @BindView(R.id.seller_imageview)
        CircleImageView sellerImage;

        private AdViewModel adViewModel;

        private Subject<AdViewModel> sellerClickSubject;
        private Subject<AdViewModel> favouriteClickSubject;
        private Subject<AdViewModel> cardClickSubject;

        AdsViewHolder(@NonNull View itemView,
                      Subject<AdViewModel> sellerClickSubject,
                      Subject<AdViewModel> favouriteClickSubject,
                      Subject<AdViewModel> cardClickSubject) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.sellerClickSubject = sellerClickSubject;
            this.favouriteClickSubject = favouriteClickSubject;
            this.cardClickSubject = cardClickSubject;
        }

        void setItem(AdViewModel adViewModel) {
            this.adViewModel = adViewModel;
            sellerName.setText(adViewModel.getSellerName());
            sellerCity.setText(adViewModel.getSellerCity());

            int secondsInDay = 86400;
            String dateAdd = adViewModel.getAddTime();
            int dateDays = Integer.parseInt(dateAdd) / secondsInDay;

            String dateCurrent = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime() / 1000);
            int currentDays = Integer.parseInt(dateCurrent) / secondsInDay;

            String result = "";
            switch (currentDays - dateDays) {
                case 0:
                    result = "Добавлено сегодня";
                    break;
                case 1:
                    result = "Добавлено вчера";
                    break;
                case 2:
                    result = "Добавлено позавчера";
                    break;
                default:
                    result = "Добавлено давным-давно";
                    break;
            }
            dateView.setText(result);



            favButton.setImageResource(adViewModel.isFavourite() ? R.drawable.ic_bookmark
                    : R.drawable.ic_bookmark_border);

            Glide.with(itemView)
                    .load(adViewModel.getPictureUrl())
                    .placeholder(R.drawable.camo3)
                    .into(adImage);

            Glide.with(itemView)
                    .load(adViewModel.getSellerUrl())
                    .into(sellerImage);
        }

        @OnClick(R.id.card)
        void onCardClick() {
            cardClickSubject.onNext(adViewModel);
        }

        @OnClick(R.id.seller_imageview)
        void onSellerImageClick() {
            sellerClickSubject.onNext(adViewModel);
        }

        @OnClick(R.id.favourite_button)
        void onFavouriteImageClick() {
            favouriteClickSubject.onNext(adViewModel);
        }
    }
}
