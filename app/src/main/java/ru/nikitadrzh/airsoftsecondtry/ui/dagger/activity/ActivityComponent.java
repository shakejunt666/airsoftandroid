package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity;

import javax.inject.Singleton;

import dagger.Component;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouriteAdsPresenter;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouritesFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsPresenter;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.AdsPresenterModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.DbModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.FavouriteAdsPresenterModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.IntentModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.ServiceModule;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module.UseCaseModule;

@Singleton
@Component(modules = {AdsPresenterModule.class, ServiceModule.class,
        UseCaseModule.class, IntentModule.class, DbModule.class, FavouriteAdsPresenterModule.class})
public interface ActivityComponent {
    void inject(AdsFragment adsFragment);

    void inject(AdsPresenter presenter);

    void inject(FavouritesFragment favouritesFragment);

    void inject(FavouriteAdsPresenter favouriteAdsPresenter);
}
