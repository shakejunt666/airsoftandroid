package ru.nikitadrzh.airsoftsecondtry.ui.ads;

import android.content.Intent;

import java.util.List;

import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

public interface BaseAdsContract {
    interface Presenter {
        void findAds();

        void connectWithSeller(AdViewModel adViewModel);
    }

    interface View {
        void showAds(List<AdViewModel> adViewModels);

        void showError(Throwable error);

        void showDialogWithSeller(Intent intent);
    }
}
