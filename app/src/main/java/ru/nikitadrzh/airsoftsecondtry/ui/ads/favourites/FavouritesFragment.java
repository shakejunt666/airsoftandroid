package ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import ru.nikitadrzh.airsoftsecondtry.R;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsAdapter;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.DaggerInitializer;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

public class FavouritesFragment extends Fragment implements FavouriteAdsContract.View {

    @Inject
    FavouriteAdsContract.Presenter presenter;

    private AdsAdapter adsAdapter;

    @BindView(R.id.favourite_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar_favourites)
    ProgressBar progressBar;

    private RecyclerView.LayoutManager favLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInitializer.getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.favourite_fragment, container, false);
        ButterKnife.bind(this, fragmentView);
        presenter.findAds();
        progressBar.setVisibility(View.VISIBLE);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("","");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("","");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("","");
    }

    @Override
    public void showAdDeleted() {
        Toast.makeText(getContext(), "Ad was deleted", Toast.LENGTH_SHORT).show();
        presenter.findAds();
    }

    @Override
    public void showAds(List<AdViewModel> adViewModels) {
        adsAdapter.onSavedAdsUpdate(adViewModels);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showDialogWithSeller(Intent intent) {
        Objects.requireNonNull(getContext()).startActivity(intent);
    }

    private void initRecyclerView() {
        if (recyclerView.getAdapter() == null) {
            adsAdapter = new AdsAdapter();
            recyclerView.setAdapter(adsAdapter);
        } else {
            adsAdapter = (AdsAdapter) recyclerView.getAdapter();
        }
        favLayoutManager = new LinearLayoutManager(null);
        recyclerView.setLayoutManager(favLayoutManager);

        Disposable disposable = adsAdapter.onSellerImageClick()
                .subscribe(this::onSellerImageClicked);

        Disposable disposable2 = adsAdapter.onFavouriteImageClick()
                .subscribe(this::onFavouriteImageClicked);
    }

    private void onFavouriteImageClicked(AdViewModel adViewModel) {
        presenter.deleteAdFromFavourites(adViewModel);
    }

    private void onSellerImageClicked(AdViewModel adViewModel) {
        presenter.connectWithSeller(adViewModel);
    }

    public AdsAdapter getAdsAdapter() {
        return adsAdapter;
    }
}
