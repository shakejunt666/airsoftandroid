package ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.module;

import dagger.Module;
import dagger.Provides;
import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManager;
import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManagerImpl;

@Module
public class IntentModule {

    @Provides
    public IntentManager provideIntentManager() {
        return new IntentManagerImpl();
    }
}
