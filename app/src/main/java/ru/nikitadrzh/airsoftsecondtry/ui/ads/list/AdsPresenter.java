package ru.nikitadrzh.airsoftsecondtry.ui.ads.list;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.mapper.AdViewModelMapper;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManager;
import ru.nikitadrzh.domain.interactor.DeleteAdFromFavouritesUseCase;
import ru.nikitadrzh.domain.interactor.SaveAdUseCase;
import ru.nikitadrzh.domain.interactor.ShowAdsUseCase;

/**
 * Presenter, отображающий список объявлений
 */
public class AdsPresenter implements AdsContract.Presenter {
    private Disposable findAdsDisposable;

    private Disposable saveAdsDisposable;

    private Disposable deleteAdDisposable;

    private Disposable saveOrDeleteDisposable;

    @Inject
    ShowAdsUseCase showAdsUseCase;

    @Inject
    SaveAdUseCase saveAdUseCase;

    @Inject
    DeleteAdFromFavouritesUseCase deleteAdFromFavouritesUseCase;

    @Inject
    AdViewModelMapper adViewModelMapper;

    @Inject
    AdMapper adMapper;

    @Inject
    IntentManager intentManager;

    @Inject
    PublishSubject<AdViewModel> subject;

    private AdsContract.View view;

    public AdsPresenter(AdsContract.View view) {
        this.view = view;
    }

    @Override
    public void findAds() {
        findAdsDisposable = showAdsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .map(adViewModelMapper::mapAdToViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showAds, this::showFindAdsError);
        saveOrDeleteDisposable = subject.subscribe(this::showSaveOrDeleteCompleted);
    }

    @Override
    public void connectWithSeller(AdViewModel adViewModel) {
        view.showDialogWithSeller(intentManager.getOpenVkIntent(adViewModel.getSellerVkId()));
    }

    @Override
    public void saveAdToFavourites(AdViewModel adViewModel) {
        saveAdsDisposable = saveAdUseCase.execute(adMapper.mapAdViewModelToAd(adViewModel))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> showSaveCompleted(adViewModel), this::showSaveAdsError);
    }

    @Override
    public void deleteAdFromFavourites(AdViewModel adViewModel) {
        deleteAdDisposable = deleteAdFromFavouritesUseCase.execute(adMapper.mapAdViewModelToAd(adViewModel))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> showDeleteCompleted(adViewModel), this::showDeleteAdsError);
    }

    private void showAds(List<AdViewModel> adsToShow) {
        view.showAds(adsToShow);
        findAdsDisposable.dispose();
    }

    private void showFindAdsError(Throwable error) {
        view.showError(error);
    }

    private void showSaveCompleted(AdViewModel adViewModel) {
        view.updateAdsList(adViewModel);
        saveAdsDisposable.dispose();
    }

    private void showSaveAdsError(Throwable error) {
        view.showError(error);
    }

    private void showDeleteCompleted(AdViewModel adViewModel) {
        view.updateAdsList(adViewModel);
        deleteAdDisposable.dispose();
    }

    private void showDeleteAdsError(Throwable error) {
        view.showError(error);
    }

    private void showSaveOrDeleteCompleted(AdViewModel adViewModel) {
        view.updateAdsList(adViewModel);
    }
}

