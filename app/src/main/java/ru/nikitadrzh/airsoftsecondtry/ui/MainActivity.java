package ru.nikitadrzh.airsoftsecondtry.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import ru.nikitadrzh.airsoftsecondtry.R;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.favourites.FavouritesFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.ads.list.AdsFragment;
import ru.nikitadrzh.airsoftsecondtry.ui.dagger.activity.DaggerInitializer;
import ru.nikitadrzh.airsoftsecondtry.ui.router.FragmentNavigator;
import ru.nikitadrzh.airsoftsecondtry.ui.router.IntentManagerImpl;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottomNavigationView)
    BottomNavigationView bottomNavigationView;

    FragmentNavigator fragmentNavigator;

    AdsFragment adsFragment;
    FavouritesFragment favouritesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
        ButterKnife.bind(this);
        initActionBar();

        fragmentNavigator.showAdsFragment(adsFragment, favouritesFragment);
        // TODO: 01.08.2019 убать в даггер
        Realm.init(this);
        //
        setUpBottomNavigationView();
    }

    private void setUpBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.favourites_button:
                    fragmentNavigator.showFavouriteAdsFragment(favouritesFragment, adsFragment);
                    break;
                case R.id.feed_button:
                    fragmentNavigator.showAdsFragment(adsFragment, favouritesFragment);
                    break;
            }
            return true;
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showNavigationAndToolbar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    private void initActionBar() {
        setSupportActionBar(toolbar);
    }

    // TODO: 17.07.2019   переместить в FragmentInitializer потом
    // TODO: 17.07.2019 Dagger 
    private void initFragment() {
        fragmentNavigator = new FragmentNavigator(
                getSupportFragmentManager(),
                this.getApplicationContext(),
                new IntentManagerImpl()
        );

        adsFragment = new AdsFragment();
        favouritesFragment = new FavouritesFragment();

        DaggerInitializer.init(adsFragment, favouritesFragment);
    }

    public void hideNavigationAndToolbar() {
        toolbar.setVisibility(View.GONE);
        bottomNavigationView.setVisibility(View.GONE);
    }

    public void showNavigationAndToolbar() {
        toolbar.setVisibility(View.VISIBLE);
        bottomNavigationView.setVisibility(View.VISIBLE);
    }
}
