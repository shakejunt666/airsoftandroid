package ru.nikitadrzh.airsoftsecondtry.ui.mapper;

import java.util.List;

import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;
import ru.nikitadrzh.domain.model.Ad;

public interface AdViewModelMapper {
    List<AdViewModel> mapAdToViewModel(List<Ad> adsToMap);
}
