package ru.nikitadrzh.airsoftsecondtry.ui.ads.list;

import android.content.Intent;

import java.util.List;

import ru.nikitadrzh.airsoftsecondtry.ui.ads.BaseAdsContract;
import ru.nikitadrzh.airsoftsecondtry.ui.model.AdViewModel;

/**
 * Интерфейс описывающий работу презентера и вью, реализуется соотвественно ими
 */
public interface AdsContract extends BaseAdsContract {

    interface Presenter extends BaseAdsContract.Presenter {
        void saveAdToFavourites(AdViewModel adViewModel);

        void deleteAdFromFavourites(AdViewModel adViewModel);
    }

    interface View extends BaseAdsContract.View {
        void showAdSaved(AdViewModel adViewModel);

        void updateAdsList(AdViewModel adViewModel);
    }
}
