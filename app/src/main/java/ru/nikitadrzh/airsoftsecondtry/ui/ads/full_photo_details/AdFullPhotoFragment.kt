package ru.nikitadrzh.airsoftsecondtry.ui.ads.full_photo_details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.ad_full_photo_fragment.*
import kotlinx.android.synthetic.main.ads_details_fragment.*
import ru.nikitadrzh.airsoftsecondtry.R

class AdFullPhotoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.ad_full_photo_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val bundle = this.arguments
        Glide.with(this)
                .load(bundle?.getString("picture"))
                .placeholder(R.drawable.camo3)
                .into(ad_full_photo)
    }
}