package ru.nikitadrzh.domain.interactor;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

public class DeleteAdFromFavouritesUseCaseTest {

    private AdRepository adRepository;
    private DeleteAdFromFavouritesUseCase deleteAdFromFavouritesUseCase;
    private TestObserver observer = new TestObserver();

    @Before
    public void setUp() {
        adRepository = Mockito.mock(AdRepository.class);
        deleteAdFromFavouritesUseCase = new DeleteAdFromFavouritesUseCase(adRepository);
    }

    @Test
    public void shouldReturnCompleteDeleting() {
        Ad ad = new Ad("", "", "",
                "", "", 0, "",
                "", "", "", 0, false);
        Mockito.when(deleteAdFromFavouritesUseCase.execute(ad)).thenReturn(Completable.complete());
        Mockito.when(adRepository.deleteAdFromFavourites(ad)).thenReturn(Completable.complete());

        deleteAdFromFavouritesUseCase.execute(ad).subscribeWith(observer).assertComplete();

        Mockito.verify(adRepository, Mockito.times(1))
                .deleteAdFromFavourites(ad);
    }

}
