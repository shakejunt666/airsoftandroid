package ru.nikitadrzh.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

public class SaveAdUseCaseTest {

    private AdRepository adRepository;
    private SaveAdUseCase saveAdUseCase;
    private TestObserver testObserver;

    @Before
    public void setUp() {
        adRepository = Mockito.mock(AdRepository.class);
        saveAdUseCase = new SaveAdUseCase(adRepository);
        testObserver = new TestObserver();
    }

    @Test
    public void shouldSaveCompleted() {
        Ad ad = new Ad("", "", "",
                "", "", 0, "",
                "", "", "", 0, false);
        Mockito.when(saveAdUseCase.execute(ad)).thenReturn(Completable.complete());
        Mockito.when(adRepository.saveAd(ad)).thenReturn(Completable.complete());

        saveAdUseCase.execute(ad).subscribeWith(testObserver).assertComplete();

        Mockito.verify(adRepository, Mockito.times(1)).saveAd(ad);
    }
}
