package ru.nikitadrzh.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

public class ShowAdsUseCaseTest {

    private AdRepository adRepository;
    private ShowAdsUseCase showAdsUseCase;

    private Ad apiAd = new Ad("description", "pictureUrl",
            "sellerName", "sellerUrl", "vkGroupName",
            1, "groupName", "groupUrl", "category",
            "addTime", 34, false);

    private Ad cacheAd = apiAd;

    private TestObserver<List<Ad>> testObserver;

    @Before
    public void setUp() {
        adRepository = Mockito.mock(AdRepository.class);
        ShowFavouriteAdsUseCase showFavouriteAdsUseCase = new ShowFavouriteAdsUseCase(adRepository);
        showAdsUseCase = new ShowAdsUseCase(adRepository, showFavouriteAdsUseCase);
        testObserver = new TestObserver<>();
    }

    @Test
    public void whenNoCache() {
        final List<Ad> apiAds = new ArrayList<>(1);
        apiAds.add(apiAd);

        final List<Ad> cacheAds = new ArrayList<>(0);

        Mockito.when(adRepository.getAds()).thenReturn(Single.just(apiAds));
        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(cacheAds));
        showAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getAds();
        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(apiAds);
    }

    @Test
    public void whenCacheIdAndApiIdNotMatches() {
        final List<Ad> apiAds = new ArrayList<>(1);
        apiAds.add(apiAd);

        final List<Ad> cacheAds = new ArrayList<>(1);
        cacheAd.setFavourite(true);
        cacheAd.setId(35);
        cacheAds.add(cacheAd);

        Mockito.when(adRepository.getAds()).thenReturn(Single.just(apiAds));
        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(cacheAds));
        showAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getAds();
        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(apiAds);
    }

    @Test
    public void whenCacheIdAndApiIdMatches() {
        final List<Ad> apiAds = new ArrayList<>(1);
        apiAds.add(apiAd);

        final List<Ad> cacheAds = new ArrayList<>(1);
        cacheAd.setId(apiAd.getId());
        cacheAd.setFavourite(true);
        cacheAds.add(cacheAd);

        Mockito.when(adRepository.getAds()).thenReturn(Single.just(apiAds));
        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(cacheAds));
        showAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getAds();
        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(cacheAds);
    }

    @Test
    public void shouldReturnEmptyListIfNoAds() {
        final List<Ad> apiAds = new ArrayList<>(0);

        final List<Ad> cacheAds = new ArrayList<>(1);
        cacheAd.setId(35);
        cacheAd.setFavourite(true);
        cacheAds.add(cacheAd);

        Mockito.when(adRepository.getAds()).thenReturn(Single.just(apiAds));
        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(cacheAds));
        showAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getAds();
        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(apiAds);
    }
}
