package ru.nikitadrzh.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

public class ShowFavouriteAdsUseCaseTest {
    private AdRepository adRepository;
    private ShowFavouriteAdsUseCase showFavouriteAdsUseCase;
    private Ad testFavouriteAd = new Ad("description", "pictureUrl",
            "sellerName", "sellerUrl", "vkGroupName",
            1, "groupName","groupUrl","category",
            "addTime", 0, false);
    private TestObserver<List<Ad>> testObserver;

    @Before
    public void setUp() {
        adRepository = Mockito.mock(AdRepository.class);
        showFavouriteAdsUseCase = new ShowFavouriteAdsUseCase(adRepository);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldReturnListOfAds() {
        final List<Ad> ads = new ArrayList<>(1);
        ads.add(testFavouriteAd);

        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(ads));
        showFavouriteAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(ads);
    }

    @Test
    public void shouldReturnEmptyListIfNoAds() {
        final List<Ad> ads = new ArrayList<>(0);

        Mockito.when(adRepository.getFavouriteAds()).thenReturn(Single.just(ads));
        showFavouriteAdsUseCase.execute().subscribe(testObserver);

        Mockito.verify(adRepository, Mockito.times(1)).getFavouriteAds();
        Mockito.verifyNoMoreInteractions(adRepository);

        testObserver.assertComplete();
        testObserver.assertValue(ads);
    }
}
