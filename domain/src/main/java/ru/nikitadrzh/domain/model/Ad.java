package ru.nikitadrzh.domain.model;

/**
 * Класс-модель объявлений, которые выдаются приложением
 */
public class Ad {

    private String description;
    private String pictureUrl;
    private Seller seller;
    private VkGroup vkGroup;
    private String category;
    private String addTime;

    private int id;
    private boolean isFavourite;

    public Ad(String description,
              String pictureUrl,
              String sellerName,
              String sellerUrl,
              String sellerCity,
              Integer sellerVkId,
              String vkGroupName,
              String vkGroupUrl,
              String category,
              String addTime,
              int id,
              boolean isFavourite) {
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.seller = new Seller();
        this.seller.sellerName = sellerName;
        this.seller.sellerUrl = sellerUrl;
        this.seller.sellerCity = sellerCity;
        this.seller.sellerVkId = sellerVkId;
        this.vkGroup = new VkGroup();
        this.vkGroup.vkGroupName = vkGroupName;
        this.vkGroup.vkGroupUrl = vkGroupUrl;
        this.category = category;
        this.addTime = addTime;
        this.id = id;
        this.isFavourite = isFavourite;
    }

    private class Seller {
        private String sellerName;
        private String sellerUrl;
        private String sellerCity;
        private Integer sellerVkId;
    }

    public Integer getSellerVkId() {
        return seller.sellerVkId;
    }

    public String getSellerCity() {
        return seller.sellerCity;
    }

    public String getSellerName() {
        return seller.sellerName;
    }

    public String getSellerUrl() {
        return seller.sellerUrl;
    }

    private class VkGroup {
        private String vkGroupName;
        private String vkGroupUrl;
    }

    public String getVkGroupName() {
        return vkGroup.vkGroupName;
    }

    public void setVkGroupName(String vkGroupName) {
        this.vkGroup.vkGroupName = vkGroupName;
    }

    public String getVkGroupUrl() {
        return vkGroup.vkGroupUrl;
    }

    public void setVkGroupUrl(String vkGroupUrl) {
        this.vkGroup.vkGroupUrl = vkGroupUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public VkGroup getVkGroup() {
        return vkGroup;
    }

    public void setVkGroup(VkGroup vkGroup) {
        this.vkGroup = vkGroup;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "description='" + description + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", seller=" + seller +
                ", vkGroup=" + vkGroup +
                ", category='" + category + '\'' +
                ", addTime='" + addTime + '\'' +
                ", id=" + id +
                ", isFavourite=" + isFavourite +
                '}';
    }
}
