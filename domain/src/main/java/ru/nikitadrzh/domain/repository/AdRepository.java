package ru.nikitadrzh.domain.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ru.nikitadrzh.domain.model.Ad;

/**
 * Интерфейс хранилища, который реализуется на внешних уровнях (Бд, интернет...)
 */
public interface AdRepository {
    Single<List<Ad>> getAds();

    Single<List<Ad>> getFavouriteAds();

    Completable saveAd(Ad ad);

    Completable deleteAdFromFavourites(Ad ad);
}
