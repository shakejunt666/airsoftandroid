package ru.nikitadrzh.domain.interactor;

import java.util.List;

import io.reactivex.Single;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

/**
 * UseCase получения сохраненных объявлений (будет использоваться для загрузки с БД)
 */
public class ShowFavouriteAdsUseCase {
    private final AdRepository adRepository;

    public ShowFavouriteAdsUseCase(AdRepository adRepository) {
        this.adRepository = adRepository;
    }

    public Single<List<Ad>> execute() {
        return adRepository.getFavouriteAds();
    }
}
