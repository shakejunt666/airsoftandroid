package ru.nikitadrzh.domain.interactor;

import io.reactivex.Completable;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

/**
 * UseCase удаления объявлений (будет использоваться для удаления из БД)
 */
public class DeleteAdFromFavouritesUseCase {
    private final AdRepository adRepository;

    public DeleteAdFromFavouritesUseCase(AdRepository adRepository) {
        this.adRepository = adRepository;
    }

    public Completable execute(Ad ad) {
        return adRepository.deleteAdFromFavourites(ad);
    }
}
