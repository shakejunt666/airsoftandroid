package ru.nikitadrzh.domain.interactor;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

/**
 * UseCase получения объявлений (будет использоваться для загрузки с сервера)
 */
public class ShowAdsUseCase {

    private final AdRepository adRepository;

    private ShowFavouriteAdsUseCase showFavouriteAdsUseCase;

    public ShowAdsUseCase(AdRepository adRepository, ShowFavouriteAdsUseCase showFavouriteAdsUseCase) {
        this.adRepository = adRepository;
        this.showFavouriteAdsUseCase = showFavouriteAdsUseCase;
    }

    public Single<List<Ad>> execute() {
        return Single.zip(adRepository.getAds(), showFavouriteAdsUseCase.execute(), new BiFunction<List<Ad>, List<Ad>, List<Ad>>() {
            @Override
            public List<Ad> apply(List<Ad> apiAds, List<Ad> cacheAds) throws Exception {
                if (!cacheAds.isEmpty()) {
                    List<Ad> checkedList = new ArrayList<>();
                    for (Ad apiAd : apiAds) {
                        for (Ad cacheAd : cacheAds) {
                            if (apiAd.getId() == cacheAd.getId()) {
                                apiAd.setFavourite(true);
                                break;
                            }
                        }
                        checkedList.add(apiAd);
                    }
                    return checkedList;
                }
                return apiAds;
            }
        });
    }
}
