package ru.nikitadrzh.domain.interactor;

import io.reactivex.Completable;
import io.reactivex.functions.Action;
import ru.nikitadrzh.domain.model.Ad;
import ru.nikitadrzh.domain.repository.AdRepository;

/**
 * UseCase сохранения объявлений (будет использоваться для сохранения в БД)
 */
public class SaveAdUseCase {
    private final AdRepository adRepository;

    public SaveAdUseCase(AdRepository adRepository) {
        this.adRepository = adRepository;
    }

    public Completable execute(Ad ad) {
        return adRepository.saveAd(ad);
    }
}
